# Manikant Jha  
**Software Engineer**  
📞 +91 8368211921 • ✉️ [mjontop0602@gmail.com](mailto:mjontop0602@gmail.com) •  
[LinkedIn](https://linkedin.com/in/manikant-jha/) • [GitHub](https://github.com/mjontop)

---

## Experience

### **Genpact**  
**Software Engineer**  
*Noida, India*  
_March 2024 – Present_  
- Developed a service to automatically perform daily unit tests on a product in development, reducing time required for bug identification and fixes.  
- Incorporated Python and PowerShell scripts to aggregate XML test results into an organized format and load the latest build code onto hardware for automated daily testing.  

---

### **Caw Studios**  
**Software Development Engineer**  
*Hyderabad, India*  
_May 2022 – March 2024_  
- Contributed to the development of **hBits**, a prop-tech product, by managing the channel partner and ops-team module (backend and frontend), used by over 50k investors.  
- Migrated legacy v1 data from MongoDB to PostgreSQL, removing 40% of redundant data.  

---

### **Quantamix Solutions**  
**Software Development Engineer**  
*Amsterdam, Netherlands*  
_March 2021 – April 2022_  
- Developed scalable React and Next.js features for improved user experience.  
- Optimized front-end performance by implementing lazy loading for images and assets, improving page load times by 15%.  

---

### **Celebrity School**  
**Software Development Engineer**  
*Mumbai, India*  
_September 2020 – February 2021_  
- Developed **CelebritySchool.in**, an EdTech platform, with a focus on seamless user experience.  
- Built robust login and sign-up features, supporting 1M+ active monthly users.  

---

## Projects

### **Ticket Price Calculator App**  
*Java, Android Studio*  
_November 2020_  
- Processed user inputs in the backend to calculate and return ticket price subtotals.  
- Designed and implemented a user interface using Android Studio’s layout editor, enabling interaction across different app scenes.  

---

## Education

### **Netaji Subhas University of Technology**  
**B.Tech in Electronics and Communications Engineering**  
*New Delhi, India*  
_July 2018 – July 2022_  

---

## Technical Skills

- **Languages & Frameworks**: TypeScript, JavaScript, React, Redux, Next.js, Nest.js, Node.js, Express.js, HTML, CSS  
- **Database**: MySQL, PostgreSQL, MongoDB  
- **DevOps**: Docker, Git, GitHub, REST APIs, AWS, Azure  
- **Others**: PowerShell, Python  

---

## Languages

- **English**  
- **Hindi**
